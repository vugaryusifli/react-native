import React from 'react';
import {StyleSheet, TextInput, View, Button, Alert} from 'react-native';

const AddTodo = ({onSubmit}) => {
    const [value, setValue] = React.useState('');

    const pressHandler = () => {
        if (value.trim()) {
            onSubmit(value);
            setValue('');
        } else {
            Alert.alert("Can't be empty");
        }

    };

    return (
        <View style={styles.block}>
            <TextInput
                style={styles.input}
                value={value}
                placeholder="Enter the value"
                onChangeText={setValue}
                autoCorrect={false}
                autoCapitalize="none"
                // keyboardType="number-pad"
            />
            <Button title="Add" onPress={pressHandler}/>
        </View>
    );
};

export default AddTodo;

const styles = StyleSheet.create({
    block: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: 15
    },
    input: {
        width: '70%',
        padding: 10,
        borderStyle: 'solid',
        borderBottomWidth: 2,
        borderBottomColor: '#3949ab'
    }
});
