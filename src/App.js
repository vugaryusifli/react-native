// import { StatusBar } from 'expo-status-bar';
import React from 'react';
import {StyleSheet, View, FlatList, Alert} from 'react-native';
import Navbar from "./Navbar/Navbar";
import AddTodo from "./AddTodo/AddTodo";
import Todo from "./Todo/Todo";

const App = () => {
    const [todos, setTodos] = React.useState([
        // {id: 1, title: 'test'},
        // {id: 2, title: 'test'},
        // {id: 3, title: 'test'},
        // {id: 4, title: 'test'},
        // {id: 5, title: 'test'},
        // {id: 6, title: 'test'},
        // {id: 7, title: 'test'},
        // {id: 8, title: 'test'},
        // {id: 9, title: 'test'}
    ]);

    const addTodo = (title) => {
        const newTodo = {
            id: Date.now().toString(),
            title
        };

        setTodos(prevTodos => [...prevTodos, newTodo])
    };

    const removeTodo = id => {
        Alert.alert(
            "Delete row",
            `Are you sure delete this row? ${id}`,
            [
                {
                    text: "Cancel",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                },
                { text: "OK", onPress: () => setTodos(prev => prev.filter(todo => todo.id !== id)) }
            ],
            { cancelable: false }
        );
    };

    return (
        <View>
            <Navbar title="Todo App!"/>
            <View style={styles.container}>
                <AddTodo onSubmit={addTodo}/>

                <FlatList
                    keyExtractor={item => item.id.toString()}
                    data={todos}
                    renderItem={({ item, index, separators }) => <Todo todo={item} onRemove={removeTodo} />}
                />
            </View>
            {/*<StatusBar style="auto" />*/}
        </View>
    );
};

export default App;

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 30,
        paddingVertical: 20
    }
});